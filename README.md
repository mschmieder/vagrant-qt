Vagrant project that uses puppet-qt module

# Requirements
## Vargant Plugins
Make sure you install all these plugins

  - vagrant-berkshelf
  - vagrant-omnibus
  - vagrant-share
  - vagrant-vbguest 

This can be checked by running the following command
```bash
vagrant plugin list
```

To install a missing plugin type:
```bash
vagrant plugin YOUR_PLUGIN_NAME
```

## Puppet Modules
   - puppet-wget
   - puppet-stdlib
   - puppet-apt
   - puppet-qt

# Supported Systems
The following systems are supported. 

   - Debian
   - Ubuntu
   - Windows (work in progress)

# Getting startet
Use your favorite terminal application an change to the project directory. Use the following command to startup vagrant and provision the
VM with qt

```bash
vagrant up --provision
```

To stop the virtual machine use
```bash
vagrant halt
```

# License

Copyright Matthias Schmieder

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

