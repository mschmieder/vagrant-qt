#!/usr/bin/env bash
set -e

echo "Bootstrap: installing puppet..."

if [ "$EUID" -ne "0" ] ; then
        echo "Script must be run as root." >&2
        exit 1
fi

if which puppet > /dev/null ; then
        echo -e "\t puppet is already installed. exiting..."
else
  echo -e "\t running apt-get update..."
  apt-get update -qq

  echo -e "\t running apt-get install puppet..."
  apt-get install -y -q puppet

  echo -e "\t successfully installed puppet. You can now start provisioning."
fi

exit 0